import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  // {
  //   path: '/about',
  //   name: 'about',
  //   // route level code-splitting
  //   // this generates a separate chunk (about.[hash].js) for this route
  //   // which is lazy-loaded when the route is visited.
  //   component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  // }
  {
    path: "/", redirect:"/dataService"
  },
  {
    path: '/dataService',
    name: 'dataService',
    component: () => import(/* webpackChunkName: "dataService" */ '../components/dataService.vue')
  },
  {
    path: '/dataPollution',
    name: 'dataPollution',
    component: () => import(/* webpackChunkName: "dataPollution" */ '../components/dataPollution.vue')
  },
  {
    path: '/dataShow',
    name: 'dataShow',
    component: () => import(/* webpackChunkName: "dataShow" */ '../components/dataShow.vue')
  },
  {
    path: '/dataAll',
    name: 'dataAll',
    component: () => import(/* webpackChunkName: "dataAll" */ '../components/dataAll.vue')
  },
]

const router = new VueRouter({
  routes
})

export default router
